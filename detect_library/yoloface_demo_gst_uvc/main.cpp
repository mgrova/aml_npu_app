#include <iostream>
#include <fstream>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <dirent.h>
#include <queue>
#include <fcntl.h>
#include <string.h>
#include <linux/fb.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <VX/vx.h>
#include <VX/vxu.h>
#include <VX/vx_api.h>
#include <VX/vx_khr_cnn.h>
#include <semaphore.h>
#include <sys/time.h>
#include <sched.h>
#include <linux/videodev2.h>
#include <poll.h>
#include <semaphore.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "nn_detect.h"
#include "nn_detect_utils.h"

#define FASTCOM_HAS_OPENCV 1
#include <fastcom/fastcom.h>


using namespace std;
using namespace cv;

#define BUFFER_COUNT 6
#define MAX_HEIGHT 1920
#define MAX_WIDTH 1080

typedef struct __video_buffer
{
	void *start;
	size_t length;

} video_buf_t;

struct Frame
{
	size_t length;
	int height;
	int width;
	unsigned char data[MAX_HEIGHT * MAX_WIDTH * 3];
} frame;

const char *xcmd = "echo 1080p60hz > /sys/class/display/mode;\
fbset -fb /dev/fb0 -g 1920 1080 1920 2160 32;\
echo 1 > /sys/class/graphics/fb0/freescale_mode;\
echo 0 0 1919 1079 >  /sys/class/graphics/fb0/window_axis;\
echo 0 0 1919 1079 > /sys/class/graphics/fb0/free_scale_axis;\
echo 0x10001 > /sys/class/graphics/fb0/free_scale;\
echo 0 > /sys/class/graphics/fb0/blank;";

int opencv_ok = 0;

pthread_mutex_t mutex4q;

unsigned char *displaybuf;
int g_nn_height, g_nn_width, g_nn_channel;
det_model_type g_model_type;
static unsigned int tmpVal;

int g_device_id;
std::string g_storeFolder;
unsigned int g_storeIndex = 0;

fastcom::ImagePublisher publisher(8888);

inline void do_mkdir(std::string _filename) {
    mkdir(_filename.c_str(), 0700);
}
// -------------------------------------------------------------------------------------------------------------------
#define _CHECK_STATUS_(status, stat, lbl)                                      \
	do                                                                         \
	{                                                                          \
		if (status != stat)                                                    \
		{                                                                      \
			cout << "_CHECK_STATUS_ File" << __FUNCTION__ << __LINE__ << endl; \
		}                                                                      \
		goto lbl;                                                              \
	} while (0)

// -------------------------------------------------------------------------------------------------------------------
int minmax(int min, int v, int max)
{
	return (v < min) ? min : (max < v) ? max : v;
}

// -------------------------------------------------------------------------------------------------------------------
static void draw_results(IplImage *pImg, DetectResult resultData, int img_width, int img_height, det_model_type type)
{
	int i = 0;
	float left, right, top, bottom;
	CvFont font;
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 3, 8);

	//	cout << "\nresultData.detect_num=" << resultData.detect_num <<endl;
	//	cout << "result type is " << resultData.point[i].type << endl;

	for (i = 0; i < resultData.detect_num; i++)
	{
		bool isPerson = false;
		if (resultData.result_name[i].lable_id == 0)
			isPerson = true;

		if(isPerson){
			left = resultData.point[i].point.rectPoint.left * img_width;
			right = resultData.point[i].point.rectPoint.right * img_width;
			top = resultData.point[i].point.rectPoint.top * img_height;
			bottom = resultData.point[i].point.rectPoint.bottom * img_height;

			//		cout << "i:" <<resultData.detect_num <<" left:" << left <<" right:" << right << " top:" << top << " bottom:" << bottom <<endl;
			CvPoint pt1;
			CvPoint pt2;
			pt1 = cvPoint(left, top);
			pt2 = cvPoint(right, bottom);

			cvRectangle(pImg, pt1, pt2, CV_RGB(255, 10, 10), 3, 4, 0);
			switch (type)
			{
			case DET_YOLOFACE_V2:
				break;
			case DET_MTCNN_V1:
			{
				int j = 0;
				cv::Mat testImage;
				testImage = cv::cvarrToMat(pImg);
				for (j = 0; j < 5; j++)
				{
					cv::circle(testImage, cv::Point(resultData.point[i].tpts.floatX[j] * img_width, resultData.point[i].tpts.floatY[j] * img_height), 2, cv::Scalar(0, 255, 255), 2);
				}
				break;
			}
			case DET_YOLO_V2:
			case DET_YOLO_V3:
			case DET_YOLO_V3_TINY:
			{
				if (top < 50)
				{
					top = 50;
					left += 10;
					//	cout << "left:" << left << " top-10:" << top-10 <<endl;
				}
				// cvPutText(pImg, resultData.result_name[i].lable_name, cvPoint(left, top - 10), &font, CV_RGB(0, 255, 0));
				cvPutText(pImg, "person", cvPoint(left, top - 10), &font, CV_RGB(0, 255, 0));
				break;
			}
			default:
				break;
			}
		}	
	}
}

// -------------------------------------------------------------------------------------------------------------------
int run_detect_model(det_model_type type)
{
	int ret = 0;
	int nn_height, nn_width, nn_channel, img_width, img_height;
	DetectResult resultData;

	det_set_log_config(DET_DEBUG_LEVEL_WARN, DET_LOG_TERMINAL);
	cout << "det_set_log_config Debug" << endl;

	//prepare model
	ret = det_set_model(type);
	if (ret)
	{
		cout << "det_set_model fail. ret=" << ret << endl;
		return ret;
	}
	cout << "det_set_model success!!" << endl;

	ret = det_get_model_size(type, &nn_width, &nn_height, &nn_channel);
	if (ret)
	{
		cout << "det_get_model_size fail" << endl;
		return ret;
	}

	cout << "\nmodel.width:" << nn_width << endl;
	cout << "model.height:" << nn_height << endl;
	cout << "model.channel:" << nn_channel << "\n"
		 << endl;

	g_nn_width = nn_width;
	g_nn_height = nn_height;
	g_nn_channel = nn_channel;

	return ret;
}

// -------------------------------------------------------------------------------------------------------------------
static void *thread_func(void *x)
{
	IplImage *frame2process = NULL, *frameclone = NULL;
	cv::Mat frame_in(MAX_WIDTH, MAX_HEIGHT, CV_8UC3);
	int width, height;
	bool bFrameReady = false;
	int i = 0, ret = 0;
	// FILE *tfd;
	// char gst_str[256];
	struct timeval tmsStart, tmsEnd;
	DetectResult resultData;

	int video_width, video_height;

	cv::Mat yolo_v2Image(g_nn_width, g_nn_height, CV_8UC1);

	cv::VideoCapture cap(g_device_id);

	if (!cap.isOpened())
	{
		cout << "capture device failed to open!\nTry with another video device port" << endl;

		goto out;
	}

	cout << "open video successfully!" << endl;

	// video_width = 640; video_height = 480;
	// cap.set(cv::CAP_PROP_FRAME_WIDTH, video_width); // valueX = your wanted width 
	// cap.set(cv::CAP_PROP_FRAME_HEIGHT, video_height); // valueY = your wanted heigth
	video_width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	video_height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

	printf("video_width: %d, video_height: %d\n", video_width, video_height);

	setpriority(PRIO_PROCESS, pthread_self(), -15);

	while (true)
	{
		pthread_mutex_lock(&mutex4q);

		if (opencv_ok == 1)
		{
			if (!cap.read(frame_in))
			{
				cout << "Capture read error" << std::endl;
				break;
			}
		}
		else
		{
			if (frame2process == NULL)
				// frame2process = cvCreateImage(cvSize(MAX_HEIGHT, MAX_WIDTH), IPL_DEPTH_8U, 3);
				frame2process = cvCreateImage(cvSize(video_height, video_width), IPL_DEPTH_8U, 3);
			if (frame2process == NULL)
			{
				pthread_mutex_unlock(&mutex4q);
				usleep(100000);
				//printf("can't load temp bmp in thread to parse\n");
				continue;
			}
			// if (frame2process->width != MAX_HEIGHT)
			// {
			// 	printf("read image not MAX_HEIGHT width\n");
			// 	pthread_mutex_unlock(&mutex4q);
			// 	continue;
			// }
			printf("prepare 1080p image ok\n");
			opencv_ok = 1; //zxw
		}
		pthread_mutex_unlock(&mutex4q);

		gettimeofday(&tmsStart, 0);

		*frame2process = IplImage(frame_in);
		cv::Mat sourceFrame = cvarrToMat(frame2process);

        // record clean image dataset
		if (sourceFrame.rows != 0)
            cv::imwrite(g_storeFolder + "/eye_" + std::to_string(g_storeIndex) + ".jpeg", sourceFrame);

		cv::resize(sourceFrame, yolo_v2Image, yolo_v2Image.size());
		gettimeofday(&tmsEnd, 0);
		tmpVal = 1000 * (tmsEnd.tv_sec - tmsStart.tv_sec) + (tmsEnd.tv_usec - tmsStart.tv_usec) / 1000;

		gettimeofday(&tmsStart, 0);
		int img_width = sourceFrame.cols;
		int img_height = sourceFrame.rows;

		input_image_t image;
		image.data = yolo_v2Image.data;
		image.width = yolo_v2Image.cols;
		image.height = yolo_v2Image.rows;
		image.channel = yolo_v2Image.channels();
		image.pixel_format = PIX_FMT_RGB888;

		//		cout << "Det_set_input START" << endl;
		ret = det_set_input(image, g_model_type);
		if (ret)
		{
			cout << "det_set_input fail. ret=" << ret << endl;
			det_release_model(g_model_type);
			goto out;
		}
		//		cout << "Det_set_input END" << endl;

		//		cout << "Det_get_result START" << endl;
		ret = det_get_result(&resultData, g_model_type);
		if (ret)
		{
			cout << "det_get_result fail. ret=" << ret << endl;
			det_release_model(g_model_type);
			goto out;
		}
		//		cout << "Det_get_result END" << endl;

		draw_results(frame2process, resultData, img_width, img_height, g_model_type);
		gettimeofday(&tmsEnd, 0);
		tmpVal = 1000 * (tmsEnd.tv_sec - tmsStart.tv_sec) + (tmsEnd.tv_usec - tmsStart.tv_usec) / 1000;
		if (tmpVal < 56)
			printf("FPS:%d\n", 1000 / (tmpVal + 8));

		// Show FPS in image
		// CvFont font;
		// cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 3, 8);
		// cvPutText(frame2process, std::to_string(1000 / (tmpVal + 8)).c_str(), cvPoint(100, 100), &font, CV_RGB(0, 0, 255));

		cv::Mat labeledImage;
		labeledImage = cv::cvarrToMat(frame2process);

		// record labeled image dataset
		if (labeledImage.rows != 0)
            cv::imwrite(g_storeFolder + "/detection_" + std::to_string(g_storeIndex) + ".jpeg", labeledImage);

		// Send image using fastcom
		publisher.publish(labeledImage, 30);
		sourceFrame.release();

		g_storeIndex++;
	}
out:
	cap.release();
	printf("thread_func exit\n");
}

// -------------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
	printf("CUSTOM VIGUS IMPLEMENTATION \n");

	int i;
	pthread_t tid[2];
	det_model_type type;
	if (argc < 2)
	{
		cout << "input param error" << endl;
		cout << "Usage: " << argv[0] << "<device id> <type>" << endl;
		cout << "       type: " << endl;
		cout << "       0 - Yoloface (NOT USED)" << endl;
		cout << "       1 - YoloV2" << endl;
		cout << "       2 - YoloV3" << endl;
		cout << "       3 - YoloV3-tiny" << endl;
		return -1;
	}

	system(xcmd);

	g_device_id = atoi(argv[1]);

	type = (det_model_type)atoi(argv[2]);
	g_model_type = type;
	run_detect_model(type);

	// Create storage folder
	g_storeFolder = "dataset_" + std::to_string(time(NULL));
	do_mkdir(g_storeFolder);

	pthread_mutex_init(&mutex4q, NULL);

	if (0 != pthread_create(&tid[0], NULL, thread_func, NULL))
	{
		fprintf(stderr, "Couldn't create thread func\n");
		return -1;
	}

	while (1)
	{
		for (i = 0; i < sizeof(tid) / sizeof(tid[0]); i++)
		{
			pthread_join(tid[i], NULL);
		}
		sleep(1);
	}

	return 0;
}
